import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { of } from 'rxjs/Observable/of';

import { Evenement } from '../class/evenement';
import { EvenementService } from '../evenement.service';

import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';


@Component({
  selector: 'app-evenement-search',
  templateUrl: './evenement-search.component.html',
  styleUrls: ['./evenement-search.component.css']
})
export class EvenementSearchComponent implements OnInit {

	evenements$ : Observable<Evenement[]>;
	private searchTerms = new Subject<string>();

	constructor(private evenementService : EvenementService) { }

	//push search term
	search(term: string){
		this.searchTerms.next(term);
	}

	ngOnInit() {
		this.evenements$ = this.searchTerms.pipe(
			// wait 300 ms for each keystroke before consider the search
			debounceTime(300),
			//ignore new term if same as preious value
			distinctUntilChanged(),
			//switch to new search observable each time term change
			switchMap((terms: string) => this.evenementService.searchEvenements(terms))
			)
	}

}
