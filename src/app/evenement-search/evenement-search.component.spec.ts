import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EvenementSearchComponent } from './evenement-search.component';

describe('EvenementSearchComponent', () => {
  let component: EvenementSearchComponent;
  let fixture: ComponentFixture<EvenementSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvenementSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvenementSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
