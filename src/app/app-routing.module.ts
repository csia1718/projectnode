import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EvenementsComponent } from './evenements/evenements.component';
import { EvenementDetailComponent } from './evenement-detail/evenement-detail.component';


const routes: Routes = [
  { path : '', redirectTo:'/evenements', pathMatch:'full'},
  { path: 'evenements', component: EvenementsComponent },
  { path: 'detail/:id', component: EvenementDetailComponent }
  
];

@NgModule({
  	exports:[RouterModule],
  	imports:[RouterModule.forRoot(routes)]
  	
})
export class AppRoutingModule { }
