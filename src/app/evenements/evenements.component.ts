import { Component, OnInit } from '@angular/core';
import { Evenement } from '../class/evenement';
import { EVENEMENTS } from '../mock/mock-evenements';
import { EvenementService} from '../evenement.service';


@Component({
	selector: 'app-evenements',
	templateUrl: './evenements.component.html',
	styleUrls: ['./evenements.component.css'],
	
})
export class EvenementsComponent implements OnInit {

	events : Evenement[];

	constructor(private evenementService : EvenementService) { }

	ngOnInit() {
  		this.getEvenements()
	}

	getEvenements(): void {
	  	this.evenementService.getEvenements()
	  	.subscribe(evenements => this.events = evenements);
	}

	selectedEvent : Evenement;
	onSelect(event: Evenement): void {
		this.selectedEvent = event;
	}

	delete(evenement: Evenement):void {
		this.events = this.events.filter(e => e !== evenement);
		this.evenementService.deleteEvenement(evenement).subscribe();
	}

}


