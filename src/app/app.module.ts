import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data.service';

import { AppComponent } from './app.component';
import { EvenementsComponent } from './evenements/evenements.component';
import { EvenementDetailComponent } from './evenement-detail/evenement-detail.component';
import { EvenementService} from './evenement.service';
import { AppRoutingModule } from './/app-routing.module';
import { EvenementSearchComponent } from './evenement-search/evenement-search.component';




@NgModule({
  declarations: [
    AppComponent,
    EvenementsComponent,
    EvenementDetailComponent,
    EvenementSearchComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    /*HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    )*/
  ],
  providers: [EvenementService],
  bootstrap: [AppComponent]
})
export class AppModule { }
