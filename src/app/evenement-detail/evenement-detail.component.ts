import { Component, OnInit, Input } from '@angular/core';
import { Evenement } from '../class/evenement';

import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { EvenementService }  from '../evenement.service';


@Component({
	selector: 'app-evenement-detail',
	templateUrl: './evenement-detail.component.html',
	styleUrls: ['./evenement-detail.component.css'],
})
export class EvenementDetailComponent implements OnInit {

	constructor(private route: ActivatedRoute, private evenementService: EvenementService, private location: Location) {}
	evenements: Evenement[];

	ngOnInit() : void{
		this.getEvenement();
		this.getEvenements();	
	}

	getEvenement(): void {
	  	const id = this.route.snapshot.paramMap.get('id');  	
	  	//alert(id)
	  	  if (id === "0" ){
		    this.evenement = new Evenement();
			  	
		  }else{
			  	this.evenementService.getEvenement(id)
			    .subscribe(evenement => this.evenement = evenement);
		}
	}

	getEvenements(): void {
	  	this.evenementService.getEvenements()
	  	.subscribe(evenements => this.evenements = evenements);
	}

	addEvent(nvEvent: Evenement): void {
		//nvEvent._id = this.generateId();
	  this.evenementService.addEvenement(nvEvent)
	    .subscribe(evenement => {
	      this.goBack();
	    });
	}

/*	generateId(): string {
		let id: string = "";
		  this.evenements.forEach(function (event) {
		    if( event._id > id){
		      id = event._id
		    }
		  }); 
		  return id+1
	}*/

	@Input() evenement : Evenement;

	goBack(): void{
		this.location.back()
	}

	save(): void {
	  if (this.evenement._id === "" ){
	    if (!this.evenement.nom){return;}
	    this.addEvent(this.evenement);
	  }else{
	    this.evenementService.updateEvenement(this.evenement)
	      .subscribe(() => this.goBack());
	  }
	}


}
