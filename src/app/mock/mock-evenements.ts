import { Evenement } from '../class/evenement';

export const EVENEMENTS: Evenement[] = [
  { _id: "1", nom: "Vacance à la Baule", datDeb: new Date("06/28/2018"), datFin: new Date("07/04/2018") },
  { _id: "2", nom: "Anniversaire Paul", datDeb: new Date("02/10/2018"), datFin: new Date("02/10/2018")},
  { _id: "3", nom: "Pot départ Roger", datDeb: new Date("12/04/2018"), datFin: new Date("12/04/2018")},
  { _id: "4", nom: "Grandes vacances", datDeb: new Date("06/28/2018"), datFin: new Date("07/04/2018")}
];