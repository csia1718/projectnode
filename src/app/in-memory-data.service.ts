import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const evenements = [
  { id: 1, nom: "Vacance bretagne", datDeb: new Date("2018-06-28"), datFin: new Date("07/04/2018") },
  { id: 2, nom: "Anniversaire Paul", datDeb: new Date("02/10/2018"), datFin: new Date("02/10/2018") },
  { id: 3, nom: "Pot départ Roger", datDeb: new Date("12/04/2018"), datFin: new Date("12/04/2018") },
  { id: 4, nom: "Grandes vacances", datDeb: new Date("06/28/2018"), datFin: new Date("07/04/2018") }
];
    return {evenements};
  }
}
