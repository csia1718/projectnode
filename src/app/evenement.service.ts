import { Injectable } from '@angular/core';
import { Evenement } from './class/evenement';
import { EVENEMENTS } from './mock/mock-evenements';
import { Observable } from 'rxjs/Observable'; 
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
	headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class EvenementService {
  	
  private evenementsUrl = 'http://localhost:3000/evenements';

  constructor(private http: HttpClient) { }

	private handleError<T> (operation = 'operation', result?: T) {
  		return (error: any): Observable<T> => {
    // TODO: send the error to remote logging infrastructure
    	console.error(error); // log to console instead
    // Let the app keep running by returning an empty result.
    	return of(result as T);
  		};
  	}



	getEvenements(): Observable<Evenement[]> {
  		return this.http.get<Evenement[]>(this.evenementsUrl).pipe(
	      	catchError(this.handleError('getEvenements', []))
	    );
	}

	getEvenement(id : string): Observable<Evenement>{
		const url = `${this.evenementsUrl}/${id}`;
		return this.http.get<Evenement>(url).pipe(
		   catchError(this.handleError<Evenement>(`getEvenement _id=${id}`))
		);
	}

	updateEvenement (evenement: Evenement): Observable<any> {
		return this.http.put(this.evenementsUrl, evenement, httpOptions).pipe(
			catchError(this.handleError<any>('updateEvent'))
		);
	}

	addEvenement (evenement: Evenement): Observable<Evenement> {
		  return this.http.post<Evenement>(this.evenementsUrl, evenement, httpOptions).pipe(
		    catchError(this.handleError<Evenement>('addEvent'))
		);
	}

	deleteEvenement (evenement: Evenement | number): Observable<Evenement> {
	    const id = typeof evenement === 'number' ? evenement : evenement._id;
	    const url = `${this.evenementsUrl}/${id}`;

	    return this.http.delete<Evenement>(url, httpOptions).pipe(
	      catchError(this.handleError<Evenement>('deleteEvent'))
	    );
	}

	searchEvenements(term: string): Observable<Evenement[]> {
		if (!term.trim()) {
		  return of([]);
		}
		return this.http.get<Evenement[]>(`api/evenements/?nom=${term}`).pipe(
		  catchError(this.handleError<Evenement[]>('searchEvenements', []))
		);
	}

}
